/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Serveur;
import java.io.*;
import java.net.*;
/**
 *
 * @author Asus
 */
public class Serveur {
    static final int port = 1200;
    public static void main(String[] args) throws Exception { 
        ServerSocket s = new ServerSocket(port);
        System.out.println("En attente de connexion");
        Socket socClient = s.accept();
        System.out.println("Connexion établie");
 
        BufferedReader entreeServeur = new BufferedReader(
                    new InputStreamReader(socClient.getInputStream()));
                        
        PrintWriter sortieServeur = new PrintWriter(
                new BufferedWriter(
                    new OutputStreamWriter(socClient.getOutputStream())),true);
                           
        String str = entreeServeur.readLine(); 
        int nombre=0;
        try{
        nombre=Integer.parseInt(str);
        }
        catch (NumberFormatException e){
        System.out.println("L'entrée du clavier n'est pas un entier");
        }
        for(int i=2;i<=nombre;i++){
            if(estPremier(i)){ System.out.println(i);}
        }
         entreeServeur.close();
         sortieServeur.close();
         socClient.close();
         }

         static boolean estPremier(int n)
         {
         boolean res; // Result of the test
         res = true;
         int i=2;
         while(i<=n/2 && res==true){
         if (n%2 == 0)
            res = false;
         i++;
         }
         return(res);
         } 
}

